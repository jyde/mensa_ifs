# mensa_IFS

Système itéré de fonctions (IFS) ayant pour attracteur "Mensa".

Pour exécuter l'IFS, copier le contenu du fichier texte, puis le coller dans la cellule Sage :

https://sagecell.sagemath.org/

Ensuite, évaluer le contenu.

Il est possible de personnaliser la couleur.
